package com.company;

public class Person {

        public String Name;
        public int Age;

        private String Town;

        Person (String name){
            Name = name;
        };

        Person () {
        }

        Person (String name, int age){
            Name = name;
            Age = age;
        }

        public void printPersonalData (){
            System.out.println(this.Name + "+" + this.Age + "+" + this.Town);
        }

        public void setTown(String _town) {
            Town = _town;
        }
}

