package com.company;


public class Main {

    public static void main(String[] args) {

        int[] array = {1, 4, 5};
        PrintNumber(array);
//        PrintNumber(10);

        Person anichka = new Person("Ani", 29);
        Person vlado = new Person("Vladi");
        vlado.Age = 30;
        vlado.setTown("Sofia");
        Person gosho = new Person();
        gosho.Age = 37;
        gosho.Name = "Joro";

        System.out.println(anichka.Name+ " -" +anichka.Age);
        System.out.println(vlado.Name+ "-" +vlado.Age);
        System.out.println(gosho.Name+ "-" +gosho.Age);

        printPersonsData(anichka);
        printPersonsData(vlado);
        printPersonsData(gosho);

        anichka.printPersonalData();
        vlado.printPersonalData();
        gosho.printPersonalData();


        int yearOfBirth = AgeUtility.CalculateYearOfBirth(vlado.Age);
        System.out.println(yearOfBirth);
        AgeUtility util = new AgeUtility();

    }

    public static void PrintNumber (int[] numbers){
        for (int number : numbers){
            System.out.println(number);
        }

    }

    public static void printPersonsData (Person _person){
        System.out.println(_person.Name +" "+ _person.Age);
    }

}

